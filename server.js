"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Hapi = require("hapi");
// import { Routes } from "./routes/routes";
var Mongoose = require("mongoose");
Mongoose.connect("mongodb://54.87.49.157:27017");
var server = new Hapi.Server("localhost", 8080);
var rootHandler = function (req, res) {
    res({ message: "Candy Store API" });
};
server.route({
    method: "GET",
    path: "/",
    handler: rootHandler
});
// Routes.init(server);
server.start(function () {
    console.log("Server started at: " + server.info.uri);
});
