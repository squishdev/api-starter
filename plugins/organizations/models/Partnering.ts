import {Mongoose} from "mongoose";
const Schema = Mongoose.Schema;

const PartnershipSchema = new Schema({
    firstName:  { type: String, required: true, trim: true },
    lastName:   { type: String, required: true, trim: true },
    company:    { type: String, required: true, trim: true },
    webAddress: { type: String, required: true, trim: true },
    phone:      { type: String, required: true, trim: true },
    specialty:  { type: String, required: true, trim: true },
    dateAdded:  { type: Date, required: true, default: Date.now }
});

let Partnership = Mongoose.model("partnership", PartnershipSchema);

module.exports = {
    Partnership: Partnership
};