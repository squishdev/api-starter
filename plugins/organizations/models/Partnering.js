"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var Schema = mongoose_1.Mongoose.Schema;
var PartnershipSchema = new Schema({
    firstName: { type: String, required: true, trim: true },
    lastName: { type: String, required: true, trim: true },
    company: { type: String, required: true, trim: true },
    webAddress: { type: String, required: true, trim: true },
    phone: { type: String, required: true, trim: true },
    specialty: { type: String, required: true, trim: true },
    dateAdded: { type: Date, required: true, default: Date.now }
});
var Partnership = mongoose_1.Mongoose.model("partnership", PartnershipSchema);
module.exports = {
    Partnership: Partnership
};
