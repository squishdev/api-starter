import * as Hapi from "hapi";
// import { Routes } from "./routes/routes";
import * as Mongoose from "mongoose";

Mongoose.connect("mongodb://54.87.49.157:27017");

const server = new Hapi.Server("localhost", 8080);
const rootHandler = (req, res) => {
    res({message: "Candy Store API"});
};

server.route({
    method: "GET",
    path: "/",
    handler: rootHandler
});

// Routes.init(server);

server.start(() => {
    console.log("Server started at: " + server.info.uri);
});